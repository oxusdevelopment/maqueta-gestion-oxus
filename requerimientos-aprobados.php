<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Alza de Isapres | Starter</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="dist/css/AdminLTE.min.css">

        <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

        <link rel="stylesheet" href="dist/css/style.css">
    </head>
    <!-- Las opciones de los colores del administrador estan dentro de la carpeta dist/css/skins -->
    <!-- Solo elegir la de mas agrado y cambiar la clase de body por skin-ColorDeseadoHeader -->
    <!-- Si solo se deja la clase asi el unico color que cambia es el del header, para cambiar el color del sidebar -->
    <!-- Se deja vacio para negro y  para blanco se le coloca "-light" -->
    <body class="skin-blue-light sidebar-mini sidebar-collapse">
        <div class="wrapper">

            <?php include('elements/header.php'); ?>
            <?php include('elements/sidebar.php'); ?>

            <div class="content-wrapper">

                <section class="content-header">
                    <h1>
                        Header de la página
                        <small>Descripción opcional</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Nivel</a></li>
                        <li class="active">Aquí</li>
                    </ol>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-3 col-md-push-9">
                            <div class="box box-default"><!-- filtro -->
                                <div class="box-header with-border">
                                    <h3 class="box-title">Buscar</h3>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <form class="" action="" method="post">
                                            <div class="form-group">
                                                    <label>ID</label>
                                                    <input type="text" name="" value="" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>Nombre Cliente</label>
                                                    <input type="text" name="" value="" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label>Necesita cotización</label>
                                                            <select class="form-control" name="">
                                                                <option value="" class="">Si</option>
                                                                <option value="" class="">No</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label>Aprobado por el cliente</label>
                                                            <select class="form-control" name="">
                                                                <option value="" class="">Si</option>
                                                                <option value="" class="">No</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <button type="submit" name="button" class="btn btn-info">Filtrar</button>
                                                <a href="#" class="btn btn-default pull-right">Limpiar</a>
                                            </form>
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9 col-md-pull-3">
                        <div class="box box-default">
                            <div class="box-header with-border">
                                <h3 class="box-title">Requerimientos aprobados</h3>
                            </div>
                            <div class="box-body">
                                <div class="table-responsive">
                                    <table class="table table-condensed text-center">
                                        <thead>
                                            <th>ID</th>
                                            <th>Cliente</th>
                                            <th>Contacto</th>
                                            <th>Prioridad</th>
                                            <th>Tipo</th>
                                            <th>Presupuesto</th>
                                            <th>Aprobado</th>
                                            <th>Necesita Cotizacion</th>
                                            <th>Acciones</th>
                                        </thead>
                                        <tbody>
                                        <tr>
                                                <td>10303456</td>
                                                <td>Nombre Genérico</td>
                                                <td>+562 20303456</td>
                                                <td>Urgencia</td>
                                                <td>Mantención</td>
                                                <td>$ Mucho</td>
                                                <td>Si</td>
                                                <td><i class="fa fa-circle red"></i></td>
                                                <td>
                                                    <div class="box-tools">
                                                        <a href="requerimiento-editar.php" class="btn btn-box-tool" title="Ver requerimiento"><i class="fa fa-eye"></i></a>
                                                        <a href="tarea-agregar.php" class="btn btn-box-tool" title="Agregar tarea"><i class="fa fa-plus"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>10303456</td>
                                                <td>Nombre Genérico</td>
                                                <td>+562 20303456</td>
                                                <td>Urgencia</td>
                                                <td>Mantención</td>
                                                <td>$ Mucho</td>
                                                <td>No</td>
                                                <td><i class="fa fa-circle red"></i></td>
                                                <td>
                                                    <div class="box-tools">
                                                        <a href="requerimiento-editar.php" class="btn btn-box-tool" title="Ver requerimiento"><i class="fa fa-eye"></i></a>
                                                        <a href="tarea-agregar.php" class="btn btn-box-tool" title="Agregar tarea"><i class="fa fa-plus"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>10303456</td>
                                                <td>Nombre Genérico</td>
                                                <td>+562 20303456</td>
                                                <td>Urgencia</td>
                                                <td>Mantención</td>
                                                <td>$ Mucho</td>
                                                <td>Si</td>
                                                <td><i class="fa fa-circle green"></i></td>
                                                <td>
                                                    <div class="box-tools">
                                                        <a href="requerimiento-editar.php" class="btn btn-box-tool" title="Ver requerimiento"><i class="fa fa-eye"></i></a>
                                                        <a href="tarea-agregar.php" class="btn btn-box-tool" title="Agregar tarea"><i class="fa fa-plus"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </section>
            </div>
            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    Cualquier cosa que quieras colocar
                </div>
                <strong>Copyright &copy; 2016 <a href="#">Compañia Genérica.</a></strong> Todos los derechos reservados.
            </footer>
            <div class="control-sidebar-bg"></div>
            <div id="modalContacto" tabindex="-1" role="dialog" class="modal fade in">
                <div role="document" class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <h3>Agregar contacto con el cliente</h4>
                            <div class="table-responsive">
                                <table class="table table-condensed">
                                    <thead>
                                        <th>Forma contacto</th>
                                        <th>Comentario</th>
                                        <th>Guardar</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <select class="form-control input-sm" name="">
                                                    <option value="" class="">forma 1</option>
                                                    <option value="" class="">forma 2</option>
                                                    <option value="" class="">forma 3</option>
                                                    <option value="" class="">forma 4</option>
                                                    <option value="" class="">forma 5</option>
                                                </select>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <input type="text" name="" value="" class="form-control input-sm">
                                                </div>
                                            </td>
                                            <td>
                                                <div class="box-tools width-auto">
                                                    <a href="#" class="btn btn-box-tool" title="Ver detalle"><i class="fa fa-save"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <h4>Registo de contactos con el cliente</h3>
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover text-center">
                                    <thead>
                                        <th>Nombre</th>
                                        <th>Fecha</th>
                                        <th>Forma contacto</th>
                                        <th>Comentario</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><a href="#" class="drop-info" title="Ver detalles cliente">Nombre Genérico</a></td>
                                            <td>2017-10-5 13:1</td>
                                            <td>Correo electronico</td>
                                            <td>Texto Generico Texto Generico Texto Generico Texto Generico</td>
                                        </tr>
                                        <tr>
                                            <td><a href="#" class="drop-info" title="Ver detalles cliente">Nombre Genérico</a></td>
                                            <td>2017-10-5 13:1</td>
                                            <td>Llamada</td>
                                            <td>Texto Generico Texto Generico Texto Generico Texto Generico</td>
                                        </tr>
                                        <tr>
                                            <td><a href="#" class="drop-info" title="Ver detalles cliente">Nombre Genérico</a></td>
                                            <td>2017-10-5 13:1</td>
                                            <td>Whatsapp</td>
                                            <td>Texto Generico Texto Generico Texto Generico Texto Generico</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- REQUIRED JS SCRIPTS -->

        <!-- jQuery 2.2.3 -->
        <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
        <!-- Bootstrap 3.3.6 -->
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <!-- AdminLTE App -->
        <script src="dist/js/app.min.js"></script>

        <script type="text/javascript">
            $('.drop-info').click(function(){
                $('.info-client').removeClass('hide');
            });
        </script>
    </body>
</html>
