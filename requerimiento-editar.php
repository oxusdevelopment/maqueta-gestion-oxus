<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Alza de Isapres | Starter</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="dist/css/AdminLTE.min.css">

        <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

        <link rel="stylesheet" href="dist/css/style.css">
    </head>
    <!-- Las opciones de los colores del administrador estan dentro de la carpeta dist/css/skins -->
    <!-- Solo elegir la de mas agrado y cambiar la clase de body por skin-ColorDeseadoHeader -->
    <!-- Si solo se deja la clase asi el unico color que cambia es el del header, para cambiar el color del sidebar -->
    <!-- Se deja vacio para negro y  para blanco se le coloca "-light" -->
    <body class="skin-blue-light sidebar-mini sidebar-collapse">
        <div class="wrapper">

            <?php include('elements/header.php'); ?>
            <?php include('elements/sidebar.php'); ?>

            <div class="content-wrapper">

                <section class="content-header">
                    <h1>
                        Header de la página
                        <small>Descripción opcional</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Nivel</a></li>
                        <li class="active">Aquí</li>
                    </ol>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-3 col-md-push-9">

                            <div class="box box-info"><!-- Acciones -->
                                <div class="box-header with-border">
                                    <h3 class="box-title">Acciones</h3>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="btn-toolbar">
                                                <a href="javascript:history.back();" class="btn btn-info option"><i class="fa fa-repeat fa-side"></i> Volver</a>
                                                <a href="#" class="btn btn-info option"><i class="fa fa-plus fa-side"></i> Agregar tarea</a>
                                                <a href="javascript:history.back();" class="btn btn-info option"><i class="fa fa-save fa-side"></i> Guardar</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9 col-md-pull-3">
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">Editar Requerimiento</h3>
                            </div>
                            <div class="box-body">
                                <div class="row"><!--fila 1 -->
                                    <div class="col-md-4">
                                        <label>Prioridad:</label>
                                        <div class="form-group">
                                            <select name="" id="" class="form-control select-prioridad">
                                                <option value="1">Urgente</option>
                                                <option value="2">Preferente</option>
                                                <option value="3" selected>Progamado</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4 form-inicio hidden">
                                        <label>Fecha de inicio:</label>
                                        <div class="form-group">
                                            <input type="text" name="" value="10/12/18" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4 form-fin hidden">
                                        <label>Fecha Termino:</label>
                                        <div class="form-group">
                                            <input type="text" name="" value="30/12/18" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row"> <!--fila 2 -->
                                    <div class="col-md-4">
                                        <label>Tipo:</label>
                                        <div class="form-group">
                                            <select name="" id="" class="form-control select-prioridad">
                                                <option value="1">Mantención</option>
                                                <option value="2" selected>Mantención 2</option>
                                                <option value="3">Mantención 3</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Cliente:</label>
                                        <div class="form-group">
                                            <select name="" id="" class="form-control select-prioridad">
                                                <option value="1">Cliente 1</option>
                                                <option value="2">Cliente 2</option>
                                                <option value="3">Cliente 3</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Contacto:</label>
                                        <div class="form-group">
                                            <input type="text" name="" value="+569 90303456" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row"><!-- fila 3 -->
                                    <div class="col-md-4">
                                        <label>Presupuesto:</label>
                                        <div class="form-group">
                                            <div class="radio">
                                                <label style="margin-right: 10px;">
                                                    <input type="radio" name="optPresupuesto" value="si" checked>
                                                    Si
                                                </label>
                                                <label>
                                                    <input type="radio" name="optPresupuesto" value="no" >
                                                    No
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-precio">
                                        <label>Precio:</label>
                                        <div class="form-group">
                                            <input type="text" name="" value="$ Mucho" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-aprobado">
                                        <label>Aprobado:</label>
                                        <div class="form-group">
                                            <div class="radio">
                                                <label style="margin-right: 10px;">
                                                    <input type="radio" name="optAprobado" value="si" checked="">
                                                    Si
                                                </label>
                                                <label>
                                                    <input type="radio" name="optAprobado" value="no" checked="">
                                                    No
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row"><!-- fila 3 -->
                                    <div class="col-md-12">
                                        <label>Descripción:</label>
                                        <div class="form-group">
                                            <textarea name="" class="form-control" cols="30" rows="10" style="resize:none"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </section>
            </div>
            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    Cualquier cosa que quieras colocar
                </div>
                <strong>Copyright &copy; 2016 <a href="#">Compañia Genérica.</a></strong> Todos los derechos reservados.
            </footer>
        </div>
        <!-- REQUIRED JS SCRIPTS -->

        <!-- jQuery 2.2.3 -->
        <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
        <!-- Bootstrap 3.3.6 -->
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <!-- AdminLTE App -->
        <script src="dist/js/app.min.js"></script>
        <script>
            $(document).ready(function(){
                //Prioridad
                opt = $( ".select-prioridad option:selected" ).attr('value');
                    if(opt == 3){
                        $('.form-inicio').removeClass('hidden');
                        $('.form-fin').removeClass('hidden');
                    }
                    else{
                        $('.form-inicio').addClass('hidden');
                        $('.form-fin').addClass('hidden');
                    }
                    
                $('.select-prioridad').change(function(){
                    opt = $( ".select-prioridad option:selected" ).attr('value');
                    if(opt == 3){
                        $('.form-inicio').removeClass('hidden');
                        $('.form-fin').removeClass('hidden');
                    }
                    else{
                        $('.form-inicio').addClass('hidden');
                        $('.form-fin').addClass('hidden');
                    }
                });

                //Presupuesto
                radio = $( ".input:radio[name=optPresupuesto]:checked" ).attr('value');
                    if(radio.equals("si")){
                        $('.col-precio').removeClass('hidden');
                        $('.col-aprobado').removeClass('hidden');
                    }
                    else{
                        $('.col-precio').addClass('hidden');
                        $('.col-aprobado').addClass('hidden');
                    }
                    
                $('input:radio[name=optPresupuesto]').click(function(){
                    radio = $(this).attr('value');
                    if(radio == "si"){
                        $('.col-precio').removeClass('hidden');
                        $('.col-aprobado').removeClass('hidden');
                    }
                    else{
                        $('.col-precio').addClass('hidden');
                        $('.col-aprobado').addClass('hidden');
                    }
                });
            });
        </script>
        
    </body>
</html>
