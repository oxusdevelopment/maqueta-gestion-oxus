<aside class="main-sidebar">

    <section class="sidebar">

        <div class="user-panel">
            <div class="pull-left image">
                <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>Usuario Genérico</p>
                <a href="#"></i> DD/MM/AAAA</a>
            </div>
        </div>
        <ul class="sidebar-menu">
            <li class="header">HEADER</li>
            <li><a href="index.php"><i class="fa fa-link"></i> <span>Resumen general</span></a></li>
            <li class="treeview">
                <a href="#"><i class="fa fa-link"></i> <span>Requerimientos</span>
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="requerimiento-nuevo.php"><span>Nuevo requerimiento</span></a></li>
                    <li><a href="requerimientos-aprobados.php"><span>Requerimientos aprobados</span></a></li>
                </ul>
            </li>
        </ul>
    </section>
</aside>
