<header class="main-header">

    <a href="index.php" class="logo no-padding">
        <span class="logo-mini"><b>M</b><b class="color-red"></b></span>
        <span class="logo-lg"><b>Maqueta</b></span>
    </a>
    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                        <span class="hidden-xs">Usuario Genérico</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="user-header">
                            <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                            <p>
                                Usuario Genérico - Cargo Genérico
                                <small>Miembro desde Nov. 2012</small>
                            </p>
                        </li>
                        <li class="user-body">
                            <div class="row">
                                <div class="col-xs-4 text-center">
                                    <a href="#">Opcion 1</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#">Opcion 2</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#">Opcion 3</a>
                                </div>
                            </div>
                        </li>
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">Perfíl</a>
                            </div>
                            <div class="pull-right">
                                <a href="#" class="btn btn-default btn-flat">Salir</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
